# PHP Orientado a Objetos.

En el mencionado repositorio se encuentran programas básicos en donde se abordan temas como:

- *Clases y Objetos*
- *Métodos y atributos*
- *Constructores y destrucutores*
- *Herencia*

## Autor
- Cruz González Irvin Javier

## Asesores
- Daniel Barajas González
- Hugo German Cuellar Martínez
- Karla Fonseca
