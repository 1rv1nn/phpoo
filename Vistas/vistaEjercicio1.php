<?php

class Carro{
	//declaracion de propiedades
	public $color;
	
}

//crea aqui la clase Moto junto con dos propiedades public

class Moto{
	/*Atributo encargado del modelo de una motocicleta */
	public $modeloMoto;
	/*Atributo encargado del color de una motocicleta */
	public $colorMoto;
}


//inicializamos el mensaje que lanzara el servidor con vacio
$mensajeServidor='';
$mensajeServidor1='';
$mensajeServidor2='';


//crea aqui la instancia o el objeto de la clase Moto
$Moto1=new Moto;

$Carro1 = new Carro;

 if ( !empty($_POST)){

 	//almacenamos el valor mandado por POST en el atributo color
 	$Carro1->color=$_POST['color'];
 	//se construye el mensaje que sera lanzado por el servidor
 	$mensajeServidor='el servidor dice que ya escogiste un color: '.$_POST['color'];

 	 // recibe aqui los valores mandados por post
	$Moto1->colorMoto=$_POST['colorMoto'];
	$mensajeServidor1='el servidor dice que ya especificaste el peso de la moto: '.$_POST['colorMoto'];

	$Moto1->modeloMoto=$_POST['modeloMoto'];
	$mensajeServidor2='el servidor dice que ya especificaste el modelo de la moto: '.$_POST['modeloMoto'];
   
 }  

?>

<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-grid.css">
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>
<body>
	
	<input type="text" class="form-control" value="<?php  echo $mensajeServidor; ?>" readonly>

	<input type="text" class="form-control" value="<?php  echo $mensajeServidor1; ?>" readonly>

	<input type="text" class="form-control" value="<?php  echo $mensajeServidor2; ?>" readonly>
	<div class="container" style="margin-top: 4em">
	
	<header> <h1>Carro y Moto</h1></header><br>
	<form method="post">
		<div class="form-group row">

			 <label class="col-sm-3" for="CajaTexto1">Color del carro:</label>
			 <div class="col-sm-4">
					<input class="form-control" type="color" name="color" id="CajaTexto1">
			</div>
			<div class="col-sm-4">
			</div>

			<label class="col-sm-3" for="CajaTexto2">Color de la moto:</label>
			 <div class="col-sm-4">
					<input class="form-control" type="color" name="colorMoto" id="CajaTexto2">
			</div>
			<div class="col-sm-4">
			</div>

			<label class="col-sm-3" for="CajaTexto3">Modelo de la moto:</label>
			 <div class="col-sm-4">
					<input class="form-control" type="text" name="modeloMoto" id="CajaTexto3">
			
		</div>
		<button class="btn btn-primary" type="submit" >enviar</button>
		<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
	</form>

	</div>


</body>
</html>

