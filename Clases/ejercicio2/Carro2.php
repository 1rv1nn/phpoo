<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;

	public $anioFabricacion;

	private $revision;

	//declaracion del método verificación
	public function verificacion($anioFabricacion){
	if ($anioFabricacion>=1990 && $anioFabricacion <=2010) {
		$this->revision='Revisión';
	}elseif($anioFabricacion<=1990){
		$this->revision='No';
	}else{
		$this->revision='Sí';
	}
	}

	public function __toString(){
		return $this->revision;
	}
}



//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->anioFabricacion=$_POST['anioFabricacion'];
}

/*Llamada al método "verificación" para que el objeto "Carro1" obtenga el valor que le corresponda a su propiedad "revision" */
$Carro1 -> verificacion($Carro1->anioFabricacion);

?>
