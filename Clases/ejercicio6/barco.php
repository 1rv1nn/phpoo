<?php
include_once('transporte.php');

/**
 * Subclase que hereda de la clase transporte donde se modela el commportamiento de un barco. 
 * @author Cruz Gonzalez Irvin Javier
 * 
 */

class barco extends transporte{
    private $calado;

    //sobreescritura de constructor
    public function __construct($nom,$vel,$com,$cal){
        parent::__construct($nom,$vel,$com);
        $this->calado=$cal;
    }

    // sobreescritura de metodo
    public function resumenBarco(){
        $mensaje=parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Calado:</td>
                    <td>'. $this->calado.'</td>				
                </tr>';
        return $mensaje;
    }
}

$mensaje='';

 if (!empty($_POST) &&  $_POST['tipo_transporte']=='maritimo') {
    $bergatin1= new barco('bergatin','40','na','15');
    $mensaje=$bergatin1->resumenBarco();
 }

?>