<?php
include_once('transporte.php');
/**
 * Subclase que hereda de la clase transporte donde se modela el commportamiento de un tren. 
 * @author Cruz Gonzalez Irvin Javier
 * 
 */

 class trenGalactico extends transporte{

    private $numero_vagones;

    private $max_pasajeros;

    /**Método constructor */

    public function __construct($nom,$vel,$com,$vag,$max){
        //Sobreescritura construcutor
        parent::__construct($nom,$vel,$com);
        $this->numero_vagones=$vag;
        $this->max_pasajeros=$max;
    }

    /*Declaración del método */
    public function resumenTrenGalactico(){
       //Soobreescritura del método crear_ficha en la clase padre.

       $mensaje=parent::crear_ficha();
       $mensaje.='<tr>
                 <td>  Numero de vagones:</td>
                 <td>'. $this->numero_vagones.'</td>
                 </tr>
                 
                 <tr>
						<td>Número de pasajeros maximos:</td>
						<td>'. $this->max_pasajeros.'</td>				

                </tr>';
        return $mensaje;
    }
 }

 $mensaje='';
 
?>